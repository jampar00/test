﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            //Hello World!
            Console.Write("Please type your name: ");
            var name = Console.ReadLine();
            var date = DateTime.Now;
            Console.WriteLine($"\nToday is: {date:d} at {date:t}");
            Console.WriteLine($"\nHello {name}, Happy Coding!");
        

            //calculation
            int num1, num2, sum;
            Console.Write("\n\nEnter first number: ");  //use writeline for new line
            num1 = int.Parse(Console.ReadLine());
            Console.Write("Enter second number: ");
            num2 = int.Parse(Console.ReadLine());
            sum = num1 + num2;

            Console.WriteLine($"The sum is: {sum}");  // or: Console.WriteLine(sum);
           
            Console.Write("\nPress any key to exit....");
            Console.ReadKey();

           

        }
    }
}
